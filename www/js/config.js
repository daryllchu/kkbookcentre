angular.module('app')

.constant('Shop', {
  version                             : '1.75',
  name                                : 'KK Book Centre',
  URL                                 : 'http://www.kkbookcentre.com/',
  EMAIL                               : 'kkbookcentre@yahoo.com',
  ConsumerKey                         : 'ck_3422f4eb18f0192eeeb46af68bd994d68232ac10', // Get this from your WooCommerce
  ConsumerSecret                      : 'cs_2a4b92e69634fa2366086088d62b5c83a3f62b8f', // Get this from your WooCommerce

  homeSlider                          : false, // If you dont want to use home slider, set it to FALSE
  CurrencyFormat                      : true, // If you want to use currency format, set it to TRUE
  shipping                            : [
                                          {id: 'flat_rate:4', name: 'Will contact for quote', cost: 0}
                                          // {id: 'flat_rate:3', name: 'Flat Rate', cost: 5},
                                          // {id: 'flat_rate:2', name: 'Worldwide Flat Rate', cost: 15}
                                        ],
  payment                             : [
                                          {id: 'cod', name: 'Payment method to be advised', icon: 'fa fa-money', desc: 'Payment method to be advised'}
                                          // {id: 'bacs', naclreame: 'Direct Bank Transfer', icon: 'fa fa-university', desc: 'You can pay using direct bank account'},
                                          // {id: 'paypal', name: 'Paypal', icon: 'fa fa-cc-paypal', desc: 'You can pay via Paypal and Credit Card'},
                                          // {id: 'razor', name: 'RazorPay', icon: 'fa fa-money', desc: 'Pay with RazorPay for Indian region only'} // Only for Indian currency (INR = Indian Rupee)
                                        ],

  GCM_SenderID                        : 'xxxxxxxxxxxx', // Get this from https://console.developers.google.com
  OneSignalAppID                      : 'xxxxxxxxxxxx', // Get this from https://onesignal.com

                                        // Change this Paypal Sandbox and LIVE with yours
  payPalSandboxClientID               : 'AZjyISbp1zmOhZ0o_iAG3W2IGjlz2hvEC-8cGoQ7fXcMFN9afaRuW0X1B1PVSgkSuTQWOKqM9N4NTkOP',
  payPalLiveClientID                  : 'xxxxxxxxxxxx',
  payPalEnv                           : 'PayPalEnvironmentSandbox', // to go live, use this: 'PayPalEnvironmentProduction'

                                        // RazorPay only can be used for Indian currency (INR = Indian Rupee)
                                        // If you want use LIVE, get your LIVE key from RazorPay Dashboard and use it here
  RazorKeyId                          : 'rzp_test_A5RTlFgqFuNdJU',  // Get this from https://dashboard.razorpay.com
  RazorSecretKey                      : 'JE7YydcgwT8dPhIVEFr60pDj', // Get this from https://dashboard.razorpay.com
  RazorYourLogoURL                    : 'https://goo.gl/ZwgRF9', // your logo image
  RazorThemeColor                     : '#F37254', // Theme color for RazorPay
  RazorYourShopDescription            : 'iPrima Media WooCoommerce Full App for Android & iOS' // Your shop description
})

.constant('$ionicLoadingConfig', {
  template: '<ion-spinner icon="dots"></ion-spinner>',
})

.constant('listLang', [
    {code: 'en', text: 'English'}
    // {code: 'ar', text: 'Arabic'},
    // {code: 'pt', text: 'Portuguese'},
    // {code: 'de', text: 'German'},
    // {code: 'hi', text: 'Hindi'}
  ]
);
